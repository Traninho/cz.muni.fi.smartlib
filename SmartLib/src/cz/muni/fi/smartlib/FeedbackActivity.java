package cz.muni.fi.smartlib;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import cz.muni.fi.smartlib.model.Review;
import cz.muni.fi.smartlib.service.RESTServiceHelper;
import cz.muni.fi.smartlib.utils.UIUtils;
import cz.muni.fi.smartlib.utils.Utils;
import cz.muni.fi.smartlib.utils.Validator;

public class FeedbackActivity extends SherlockActivity implements OnClickListener {
	public static final String TAG = FeedbackActivity.class.getSimpleName();
	
	private EditText mFeedback;
	private Button mRateBtn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_feedback);
		
		ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
		
		mFeedback = (EditText) findViewById(R.id.feedback);
		mFeedback.setTextColor(Color.parseColor("#686868"));
		mRateBtn = (Button) findViewById(R.id.rate_button);
		mRateBtn.setOnClickListener(this);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case android.R.id.home:
				Utils.startMainActivity(this);
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.rate_button) {
			send();
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
	}
	

	private void send() {
		if (!Validator.isNullorEmpty(mFeedback.getText().toString())) {
			disableUI();
			
			RESTServiceHelper.getInstance(this).sendFeedback(new FeedbackReceiver(), mFeedback.getText().toString());
		} else {
			UIUtils.makeToast(this, R.string.toast_empty_fields);			
		}
	}

	private void disableUI() {
		mRateBtn.setEnabled(false);
		mFeedback.setEnabled(false);
	}
	
	private void enableUI() {	
		mRateBtn.setEnabled(true);
		mFeedback.setEnabled(true);
	}
	
	public class FeedbackReceiver extends ResultReceiver {
		private boolean enabled;
		
		public FeedbackReceiver() {
			super(new Handler());
			this.enabled = true;
		}

		public void unregister() {
			enabled = false;
		}
		
		@Override
	    protected void onReceiveResult (int resultCode, Bundle resultData) {
			if (enabled) {
				Log.i(TAG, "Result from REST "+ resultCode + "received.");
				boolean result = resultData.getBoolean(RESTServiceHelper.PARAM_RESULT);
				if (result) {
					UIUtils.makeToast(FeedbackActivity.this, R.string.crash_dialog_ok_toast);
					finish();
				} else {
					UIUtils.makeToast(FeedbackActivity.this, R.string.crash_dialog_ok_toast);
					enableUI();
					finish();
				}
			}
	    }
		
	}
	
}
